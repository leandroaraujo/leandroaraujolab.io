"use strict";
var database;

onmessage = function(event) {
    var action = event.data[0];
    var dados  = event.data;

    // abre o banco
    if( action == "open" ) {
        var request = indexedDB.open('tarefas', 3);

        request.onerror = function(e) {
            var mensagem = 'Erro no amarzenamento local.';
            postMessage(['mostraMensagem', 'danger', mensagem, true]);
        }

        request.onsuccess = function(e) {
            database = e.target.result;

            listarTarefas(database);
        }

        request.onupgradeneeded = function(e) {
            database = e.target.result;

            if( !database.objectStoreNames.contains('tarefas') ) {

                var tarefas = database.createObjectStore('tarefas', { autoIncrement: true });

                tarefas.createIndex('tarefa', 'tarefa', { unique: false });
                tarefas.createIndex('data', 'data', { unique: false });
            }

            var mensagem = 'Banco de dados criado com success.';
            postMessage(['mostraMensagem', 'success', mensagem]);
        }
    }

    // adiciona tarefa
    if( action == "adiciona" ) {
        var tarefaTitulo = dados[1];
        var tarefaDataLimite = dados[2];
        var tarefaPrioridade = dados[3];

        var tarefas = database.transaction(['tarefas'], 'readwrite').objectStore('tarefas');

        var request = tarefas.add({
            tarefa: tarefaTitulo,
            data: tarefaDataLimite,
            prioridade: tarefaPrioridade
        });

        request.onsuccess = function(event) {
            var mensagem = 'Tarefa adicionada com sucesso.';
            postMessage(['mostraMensagem', 'success', mensagem]);
            postMessage(['limpaCamposCadastro']);

            listarTarefas(database);
        }

        request.onerror =function(event) {
            var mensagem = 'Não foi possível adicionar a tarefa.';
            postMessage(['mostraMensagem', 'danger', mensagem]);
        }
    }

    // atualiza tarefa
    if( action == "atualiza" ) {
        var request = database.transaction(['tarefas'], 'readwrite').objectStore('tarefas').get(dados[1]);

        request.onsuccess = function(event) {
            var item = event.target.result;

            if(item) {
                item.tarefa     = dados[2];
                item.data       = dados[3];
                item.prioridade = dados[4];

                event.target.source.put(item, dados[1]).onsuccess = function(e) {
                    postMessage(['atualiaLinhaTabela', dados[1], item.tarefa, item.data, item.prioridade]);

                    var mensagem = 'Tafera alterada com sucesso.';
                    postMessage(['mostraMensagem', 'success', mensagem]);
                }
            } else {
                var mensagem = 'Não foi possível alterar a tarefa.<br /><small>' + event.error.name + '</small>';
                postMessage(['mostraMensagem', 'danger', mensagem, true]);
            }

            listarTarefas(database);
        }

        request.onerror = function(event) {
            var mensagem = 'Não foi possível alterar a tarefa.<br /><small>' + event.error.name + '</small>';
            postMessage(['mostraMensagem', 'danger', mensagem, true]);
        }

    }

    // remove tarefa
    if( action == "remove" ) {
        database.transaction(['tarefas'], 'readwrite').objectStore('tarefas').delete(dados[1]).onsuccess = function(event) {
            var mensagem = 'Tarefa removida com sucesso.';
            postMessage(['mostraMensagem', 'success', mensagem]);

            listarTarefas(database);
        }
    }

    // busca pelo titulo da tarefa
    if( action == "buscarPorTitulo" ) {
        var tarefaBusca  = dados[1];

        if( tarefaBusca != '' && tarefaBusca.length > 2 ) {
            listarResultado('tarefa', tarefaBusca, database);
        } else {
            var mensagem = 'Digite 3 ou mais caracteres.';
            postMessage(['mostraMensagem', 'danger', mensagem]);
        }
    }

    // busca pela data da tarefa
    if( action == "buscarPorData" ) {
        var dataBusca = dados[1];

        if( dataBusca != '' || dataBusca.length > 9 ) {
            listarResultado('data', dataBusca, database);
        } else {
            var mensagem = 'A data que você digitou é inválida.';
            postMessage(['mostraMensagem', 'danger', mensagem]);
        }
    }
}

function listarTarefas(db) {
    var itemTarefa = '';
    var abreTabela = db.transaction(['tarefas'], 'readonly').objectStore('tarefas');
    var request    = abreTabela.openCursor();

    request.onsuccess = function(event) {
        var item = event.target.result;

        if(item) {
            var tarefaCodigo     = item.key;
            var tarefaTitulo     = item.value.tarefa;
            var tarefaDataLimite = item.value.data;
            var tarefaPrioridade;

            if( item.value.prioridade == '3' ) {
                tarefaPrioridade = 'Alta';
            } else if( item.value.prioridade == '2' ) {
                tarefaPrioridade = 'Média';
            } else {
                tarefaPrioridade = 'Baixa';
            }

            itemTarefa +=  '<tr>' +
                '<td>' + tarefaCodigo + '</td>' +
                '<td id="tarefaTitulo_'     + tarefaCodigo + '">' + tarefaTitulo     + '</td>' +
                '<td id="tarefaData_'       + tarefaCodigo + '">' + converteDataParaBrasil(tarefaDataLimite) + '</td>' +
                '<td id="tarefaPrioridade_' + tarefaCodigo + '">' + tarefaPrioridade + '</td>' +
                '<td id="tarefaBotoes_'     + tarefaCodigo + '">' +
                    '<button class="btn" title="Editar" onclick="editarTarefa('   + tarefaCodigo + ')"><i class="fa fa-pencil"></i></button>' +
                    '<button class="btn btn--danger" title="Remover" onclick="removerTarefa(' + tarefaCodigo + ')"><i class="fa fa-trash"></i></button>' +
                '</td>' +
            '</tr>';

            item.continue();
        } else {
            postMessage(['atualizarListaTarefas', itemTarefa]);
        }
    }
}

function listarResultado(tipo, termoBusca, db) {
    var acessoBanco = database.transaction(['tarefas'], 'readonly').objectStore('tarefas').index(tipo);
    var retornoBusca;
    var resultado = '';

    if(tipo == 'data') {
        retornoBusca = IDBKeyRange.upperBound(termoBusca, false);
    } else if(tipo == 'tarefa') {
        var ultimaBusca  = termoBusca.substr(0, termoBusca.length-1)+String.fromCharCode(termoBusca.charCodeAt(termoBusca.length-1)+1);
        retornoBusca = IDBKeyRange.bound(termoBusca, ultimaBusca, false, true);
    }

    var request = acessoBanco.openCursor(retornoBusca);

    request.onsuccess = function(event) {
        var item = event.target.result;

        if(item) {
            var tituloCodigo     = item.primaryKey;
            var tituloTarefa     = item.value.tarefa;
            var tarefaData       = converteDataParaBrasil(item.value.data);
            var tarefaPrioridade = item.value.prioridade;

            if( tarefaPrioridade == '3' ) {
                tarefaPrioridade = 'Alta';
            } else if( tarefaPrioridade == '2' ) {
                tarefaPrioridade = 'Média';
            } else {
                tarefaPrioridade = 'Baixa';
            }

            resultado += '<li class="lista-tarefas__item">' +
                            '<strong><small>#' + tituloCodigo + '</small> ' +
                            tituloTarefa + '</strong><br />' +
                            tarefaData + ' - <i>(' + tarefaPrioridade + ')</i>' +
                        '</li>';

            item.continue();
        } else {
            postMessage(['exibirBusca', tipo, termoBusca, resultado]);
        }
    }
}

// utils
// =============================================================================
function converteDataParaBrasil(data) {
    var dataArray = data.split('-');

    return dataArray[2] + '/' + dataArray[1] + '/' + dataArray[0];
}

function converteDataParaPC(data) {
    var dataArray = data.split('/');

    return dataArray[2] + '-' + dataArray[1] + '-' + dataArray[0];
}