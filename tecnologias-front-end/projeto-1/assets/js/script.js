"use strict";

var worker;

function init() {
    worker = new Worker('assets/js/database.js');


    if(!window.indexedDB) {
        window.indexedDB      = window.indexedDB      || window.mozIndexedDB         || window.webkitIndexedDB   || window.msIndexedDB;
        window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
        window.IDBKeyRange    = window.IDBKeyRange    || window.webkitIDBKeyRange    || window.msIDBKeyRange;

        alert( 'Seu navegador não possui suporte total ao sistema. Algumas funcionalidades podem apresentar problemas durante a utilização.' );

        return;
    }

    worker.onmessage = function(event) {
        var dados = event.data;

        if( dados[0] == 'mostraMensagem' ) {
            mostraMensagem(dados[1], dados[2], dados[3]?dados[3]:'');
        }

        if( dados[0] == 'atualizarListaTarefas' ) {
            atualizarListaTarefas(dados[1]);
        }

        if( dados[0] == 'limpaCamposCadastro' ) {
            limpaCamposCadastro();
        }

        if( dados[0] == 'atualizaLinhaTabela' ) {
            atualizaLinhaTabela(dados[1], dados[2], dados[3], dados[4]);
        }

        if( dados[0] == 'exibirBusca' ) {
            exibirBusca(dados[1], dados[2], dados[3]);
        }
    }

    worker.onerror = function(event) {
        mostraMensagem('danger', 'Não foi possível realizar a operação.<br /><pre>' + event.message + '</pre>', true);
    }

    worker.postMessage(['open']);

    // seleciona section
    $('#buscaButton').on('click', mostraBusca);
    $('#cadastroButton').on('click', mostraCadastro);

    // faz acoes
    $('#adicionarButton').on('click', adicionarTarefa);
    $('#buscaTituloButton').on('click', buscarPorTitulo);
    $('#buscaDataLimiteButton').on('click', buscarPorData);
}

// seleciona secoes
// =============================================================================
function mostraBusca() {
    $('#areaTarefa').addClass('hide');
    $('#areaBusca').removeClass('hide');
}

function mostraCadastro() {
    $('#areaTarefa').removeClass('hide');
    $('#areaBusca').addClass('hide');
    $('#resultadoBusca').html('');
    $('#resultadoArea').addClass('hide');
}


// adicionar/edita/remove tarefas
// =============================================================================
function adicionarTarefa() {
    var tarefaTitulo     = $('#adicionarTituloTarefa');
    var tarefaDataLimite = $('#adicionarDataLimite');
    var tarefaPrioridade = $('#adicionarPrioridade');

    if(tarefaTitulo.val() == '' || tarefaDataLimite.val() == '') {
        mostraMensagem('danger', 'Preencha todos os campos.');
    } else {
        worker.postMessage(['adiciona', tarefaTitulo.val(), tarefaDataLimite.val(), tarefaPrioridade.val()]);
    }
}

function limpaCamposCadastro() {
    $('#adicionarTituloTarefa').val('');
    $('#adicionarDataLimite').val('');
    $('#adicionarPrioridade').val(1);
}

function atualizarListaTarefas(itens) {
    var listaTarefas = $('#listaTarefas');

    if(itens && itens != undefined) {
        $('#listaTarefas').html(itens);
    }
}

/* editar tarefas */
function editarTarefa(tarefaCodigo) {
    var tarefaTitulo     = $('#tarefaTitulo_'     + tarefaCodigo);
    var tarefaData       = $('#tarefaData_'       + tarefaCodigo);
    var tarefaPrioridade = $('#tarefaPrioridade_' + tarefaCodigo);
    var tarefaBotoes     = $('#tarefaBotoes_'     + tarefaCodigo);

    if( tarefaPrioridade.html() == 'Alta' ) {
        tarefaPrioridade.html('3');
    } else if( tarefaPrioridade.html() == 'Média' ) {
        tarefaPrioridade.html('2');
    } else {
        tarefaPrioridade.html('1');
    }

    var oldTarefaTitulo     = tarefaTitulo.html();
    var oldTarefaData       = converteDataParaPC(tarefaData.text());
    var oldTarefaPrioridade = tarefaPrioridade.html();

    tarefaTitulo.html('<label for="editarTarefaTitulo_' + tarefaCodigo + '">Editar título da tarefa:</label>' +
                      '<input id="editarTarefaTitulo_'  + tarefaCodigo + '" data-previousvalue="' + oldTarefaTitulo + '" type="text" value="' + oldTarefaTitulo + '" />');

    tarefaData.html('<label for="editarTarefaData_' + tarefaCodigo + '">Editar data limite:</label>' +
                    '<input id="editarTarefaData_'  + tarefaCodigo + '" data-previousvalue="' + oldTarefaData + '" type="date" value="' + oldTarefaData + '" />');

    tarefaPrioridade.html('<label for="editarTarefaPrioridade_' + tarefaCodigo + '">Editar prioridade</label>' +
                          '<select id="editarTarefaPrioridade_' + tarefaCodigo + '" data-previousvalue="' + oldTarefaPrioridade + '">'+
                              '<option ' + (oldTarefaPrioridade=='1' ? 'selected' : '') + ' value="1">Baixa</option>' +
                              '<option ' + (oldTarefaPrioridade=='2' ? 'selected' : '') + ' value="2">Média</option>' +
                              '<option ' + (oldTarefaPrioridade=='3' ? 'selected' : '') + ' value="3">Alta</option>' +
                          '</select>');

    tarefaBotoes.html('<button class="btn btn--success" title="Salvar" onclick="edicaoSalvar('    + tarefaCodigo + ')"><i class="fa fa-check"></i></button>' +
                      '<button class="btn btn--danger" title="Cancelar" onclick="edicaoCancelar(' + tarefaCodigo + ')"><i class="fa fa-close"></i></button>');
}

function edicaoSalvar(tarefaCodigo) {
    var tarefaTitulo     = $('#editarTarefaTitulo_'     + tarefaCodigo);
    var tarefaData       = $('#editarTarefaData_'       + tarefaCodigo);
    var tarefaPrioridade = $('#editarTarefaPrioridade_' + tarefaCodigo);
    var tarefaBotoes     = $('#tarefaBotoes_'           + tarefaCodigo);

    var novoTarefaTitulo     = $('#editarTarefaTitulo_'     + tarefaCodigo).val();
    var novoTarefaData       = $('#editarTarefaData_'       + tarefaCodigo).val();
    var novoTarefaPrioridade = $('#editarTarefaPrioridade_' + tarefaCodigo).val();

    worker.postMessage(['atualiza', tarefaCodigo, novoTarefaTitulo, novoTarefaData, novoTarefaPrioridade]);
}

function edicaoCancelar(tarefaCodigo) {
    var tarefaTitulo     = $('#tarefaTitulo_'     + tarefaCodigo);
    var tarefaData       = $('#tarefaData_'       + tarefaCodigo);
    var tarefaPrioridade = $('#tarefaPrioridade_' + tarefaCodigo);
    var tarefaBotoes     = $('#tarefaBotoes_'     + tarefaCodigo);

    var oldTarefaTitulo     = $('#editarTarefaTitulo_'     + tarefaCodigo).data('previousvalue');
    var oldTarefaData       = $('#editarTarefaData_'       + tarefaCodigo).data('previousvalue');
    var oldTarefaPrioridade = $('#editarTarefaPrioridade_' + tarefaCodigo).data('previousvalue');

    if( oldTarefaPrioridade == '3' ) {
        oldTarefaPrioridade = 'Alta';
    } else if( oldTarefaPrioridade == '2' ) {
        oldTarefaPrioridade = 'Média';
    } else {
        oldTarefaPrioridade = 'Baixa';
    }

    tarefaTitulo.html(oldTarefaTitulo);
    tarefaData.html(converteDataParaBrasil(oldTarefaData));
    tarefaPrioridade.html(oldTarefaPrioridade);
    tarefaBotoes.html('<button class="btn" title="Editar" onclick="editarTarefa(' + tarefaCodigo + ')"><i class="fa fa-pencil"></i></button>' +
                      '<button class="btn btn--danger" title="Remover" onclick="removerTarefa(' + tarefaCodigo + ')"><i class="fa fa-trash"></i></button>');
}

function atualizaLinhaTabela(tarefaCodigo, novoTitulo, novaData, novaPrioridade) {
    var tarefaTitulo     = $('#editarTarefaTitulo_'     + tarefaCodigo);
    var tarefaData       = $('#editarTarefaData_'       + tarefaCodigo);
    var tarefaPrioridade = $('#editarTarefaPrioridade_' + tarefaCodigo);
    var tarefaBotoes     = $('#tarefaBotoes_'           + tarefaCodigo);

    if( novaPrioridade == '3' ) {
        novaPrioridade = 'Alta';
    } else if( novaPrioridade == '2' ) {
        novaPrioridade = 'Média';
    } else {
        novaPrioridade = 'Baixa';
    }

    tarefaTitulo.html(novoTitulo);
    tarefaData.html(novaData);
    tarefaPrioridade.html(novaPrioridade);
    tarefaBotoes.html('<button class="btn" title="Editar" onclick="editarTarefa(' + tarefaCodigo + ')"><i class="fa fa-pencil"></i></button>' +
                      '<button class="btn btn--danger" title="Remover" onclick="removerTarefa(' + tarefaCodigo + ')"><i class="fa fa-trash"></i></button>');
}

/* remove tarefas */
function removerTarefa(tarefaCodigo) {
    worker.postMessage(['remove', tarefaCodigo]);
}


// busca
// =============================================================================
function buscarPorTitulo() {
    var tarefaBusca = $('#buscaTitulo').val();

    worker.postMessage(['buscarPorTitulo', tarefaBusca]);
}

function buscarPorData() {
    var dataBusca = $('#buscaDataLimite').val();

    worker.postMessage(['buscarPorData', dataBusca]);
}

function exibirBusca(tipo, termoBusca, resultado) {
    $('#resultadoArea').removeClass('hide');
    $('#resultadoBusca').html('');

    if(tipo == 'tarefa') {
        $('#resultadoTitulo').html('Resultado da busca para "<strong>' + termoBusca + '</strong>"');
    } else if(tipo == 'data') {
        $('#resultadoTitulo').html('Resultado da busca até "<strong>' + converteDataParaBrasil(termoBusca) + '</strong>"');
    }

    $('#resultadoBusca').html(resultado);
}


// utils
// =============================================================================
function mostraMensagem(tipo, mensagem, forever) {
    var alertArea = $('#alertarea');

    alertArea.addClass('alert--' + tipo);

    if(forever) {
        alertArea.html(mensagem);
    } else {
        // escapar strings ¬¬
        alertArea.html(mensagem + '<button class="alert__close" onclick="fecharAlerta(\'' + alertArea.selector + '\', \'' + tipo + '\')">&times;</button>');
        window.setTimeout(function() { fecharAlerta(alertArea.selector, tipo) }, 10E3);
    }
}

function fecharAlerta(elemento, tipoAlerta) {
    $(elemento).removeClass('alert--' + tipoAlerta).html('');
}

function converteDataParaBrasil(data) {
    var dataArray = data.split('-');

    return dataArray[2] + '/' + dataArray[1] + '/' + dataArray[0];
}

function converteDataParaPC(data) {
    var dataArray = data.split('/');

    return dataArray[2] + '-' + dataArray[1] + '-' + dataArray[0];
}


// inicia script
window.addEventListener('load', init(), false);