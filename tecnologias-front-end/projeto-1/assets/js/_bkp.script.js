"use strict";

var database;

function init() {

    window.indexedDB      = window.indexedDB      || window.mozIndexedDB         || window.webkitIndexedDB   || window.msIndexedDB;
    window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
    window.IDBKeyRange    = window.IDBKeyRange    || window.webkitIDBKeyRange    || window.msIDBKeyRange;

    if(!window.indexedDB) {
        alert( 'Seu navegador não possui suporte total ao sistema. Algumas funcionalidades podem apresentar problemas durante a utilização.' );

        return;
    }

    var request = indexedDB.open('tarefas', 2);

    request.onerror = function(event) {
        var mensagem = 'Erro no amarzenamento local.<br /><small>' + event.error.name + '</small>';
        mostraMensagem('danger', mensagem);
    }

    request.onsuccess = function(event) {
        database = event.target.result;

        atualizarListaTarefas();
    }

    request.onupgradeneeded = function(event) {
        database = event.target.result;

        if( !database.objectStoreNames.contains('tarefas') ) {

            var tarefas = database.createObjectStore('tarefas', { autoIncrement: true });

            tarefas.createIndex('tarefa', 'tarefa', { unique: false });
            tarefas.createIndex('data', 'data', { unique: false });

        }
    }

    $('#buscaButton').on('click', mostraBusca);
    $('#cadastroButton').on('click', mostraCadastro);
    $('#adicionarButton').on('click', adicionarTarefa);
    $('#buscaTituloButton').on('click', buscarPorTitulo);
    $('#buscaDataLimiteButton').on('click', buscarPorData);
}

function mostraBusca() {
    $('#areaBusca').removeClass('hide');
    $('#areaTarefa').addClass('hide');
}

function mostraCadastro() {
    $('#areaTarefa').removeClass('hide');
    $('#areaBusca').addClass('hide');
    $('#resultadoBusca').html('');
}


function adicionarTarefa() {
    var tarefaTitulo     = $('#adicionarTituloTarefa');
    var tarefaDataLimite = $('#adicionarDataLimite');
    var tarefaPrioridade = $('#adicionarPrioridade');

    if(tarefaTitulo.val() == '' || tarefaDataLimite.val() == '') {
        mostraMensagem('danger', 'Preencha todos os campos.');
    } else {
        var tarefas = database.transaction(['tarefas'], 'readwrite').objectStore('tarefas');

        var request = tarefas.add({
            tarefa: tarefaTitulo.val(),
            data: tarefaDataLimite.val(),
            prioridade: tarefaPrioridade.val()
        });

        request.onsuccess = function(event) {
            mostraMensagem('success', 'Tarefa adicionada com sucesso.');

            atualizarListaTarefas();

            tarefaTitulo.val('');
            tarefaDataLimite.val('');
            tarefaPrioridade.val(1);
        }

        request.onerror =function(event) {
            mostraMensagem('danger', 'Não foi possível adicionar a tarefa.');
        }
    }
}

function atualizarListaTarefas() {
    var listaTarefas = $('#listaTarefas');
    var itemTarefa = '';

    var request = database.transaction(['tarefas'], 'readonly').objectStore('tarefas').openCursor();

    request.onsuccess = function(event) {
        var item = event.target.result;

        if(item) {
            var tarefaCodigo     = item.key;
            var tarefaTitulo     = item.value.tarefa;
            var tarefaDataLimite = item.value.data;
            var tarefaPrioridade;

            if( item.value.prioridade == '3' ) {
                tarefaPrioridade = 'Alta';
            } else if( item.value.prioridade == '2' ) {
                tarefaPrioridade = 'Média';
            } else {
                tarefaPrioridade = 'Baixa';
            }

            itemTarefa +=  '<tr>' +
                '<td>' + tarefaCodigo + '</td>' +
                '<td id="tarefaTitulo_'     + tarefaCodigo + '">' + tarefaTitulo     + '</td>' +
                '<td id="tarefaData_'       + tarefaCodigo + '">' + converteDataParaBrasil(tarefaDataLimite) + '</td>' +
                '<td id="tarefaPrioridade_' + tarefaCodigo + '">' + tarefaPrioridade + '</td>' +
                '<td id="tarefaBotoes_'     + tarefaCodigo + '">' +
                    '<button class="btn" title="Editar" onclick="editarTarefa('   + tarefaCodigo + ')"><i class="fa fa-pencil"></i></button>' +
                    '<button class="btn btn--danger" title="Remover" onclick="removerTarefa(' + tarefaCodigo + ')"><i class="fa fa-trash"></i></button>' +
                '</td>' +
            '</tr>';

            item.continue();
        } else {
            listaTarefas.html(itemTarefa);
        }
    }
}

function editarTarefa(tarefaCodigo) {
    var tarefaTitulo     = $('#tarefaTitulo_'     + tarefaCodigo);
    var tarefaData       = $('#tarefaData_'       + tarefaCodigo);
    var tarefaPrioridade = $('#tarefaPrioridade_' + tarefaCodigo);
    var tarefaBotoes     = $('#tarefaBotoes_'     + tarefaCodigo);

    if( tarefaPrioridade.html() == 'Alta' ) {
        tarefaPrioridade.html('3');
    } else if( tarefaPrioridade.html() == 'Média' ) {
        tarefaPrioridade.html('2');
    } else {
        tarefaPrioridade.html('1');
    }

    var oldTarefaTitulo     = tarefaTitulo.html();
    var oldTarefaData       = converteDataParaPC(tarefaData.text());
    var oldTarefaPrioridade = tarefaPrioridade.html();

    tarefaTitulo.html('<label for="editarTarefaTitulo_' + tarefaCodigo + '">Editar título da tarefa:</label>' +
                      '<input id="editarTarefaTitulo_'  + tarefaCodigo + '" data-previousvalue="' + oldTarefaTitulo + '" type="text" value="' + oldTarefaTitulo + '" />');

    tarefaData.html('<label for="editarTarefaData_' + tarefaCodigo + '">Editar data limite:</label>' +
                    '<input id="editarTarefaData_'  + tarefaCodigo + '" data-previousvalue="' + oldTarefaData + '" type="date" value="' + oldTarefaData + '" />');

    tarefaPrioridade.html('<label for="editarTarefaPrioridade_' + tarefaCodigo + '">Editar prioridade</label>' +
                          '<select id="editarTarefaPrioridade_' + tarefaCodigo + '" data-previousvalue="' + oldTarefaPrioridade + '">'+
                              '<option ' + (oldTarefaPrioridade=='1' ? 'selected' : '') + ' value="1">Baixa</option>' +
                              '<option ' + (oldTarefaPrioridade=='2' ? 'selected' : '') + ' value="2">Média</option>' +
                              '<option ' + (oldTarefaPrioridade=='3' ? 'selected' : '') + ' value="3">Alta</option>' +
                          '</select>');

    tarefaBotoes.html('<button class="btn btn--success" title="Salvar" onclick="edicaoSalvar('    + tarefaCodigo + ')"><i class="fa fa-check"></i></button>' +
                      '<button class="btn btn--danger" title="Cancelar" onclick="edicaoCancelar(' + tarefaCodigo + ')"><i class="fa fa-close"></i></button>');
}

function edicaoSalvar(tarefaCodigo) {
    var tarefaTitulo     = $('#editarTarefaTitulo_'     + tarefaCodigo);
    var tarefaData       = $('#editarTarefaData_'       + tarefaCodigo);
    var tarefaPrioridade = $('#editarTarefaPrioridade_' + tarefaCodigo);
    var tarefaBotoes     = $('#tarefaBotoes_'           + tarefaCodigo);

    var novoTarefaTitulo     = $('#editarTarefaTitulo_'     + tarefaCodigo).val();
    var novoTarefaData       = $('#editarTarefaData_'       + tarefaCodigo).val();
    var novoTarefaPrioridade = $('#editarTarefaPrioridade_' + tarefaCodigo).val();

    var request = database.transaction(['tarefas'], 'readwrite').objectStore('tarefas').get(tarefaCodigo);

    request.onsuccess = function(event) {
        var item = event.target.result;

        if(item) {
            item.tarefa     = novoTarefaTitulo;
            item.data       = novoTarefaData;
            item.prioridade = novoTarefaPrioridade;

            event.target.source.put(item, tarefaCodigo).onsuccess = function(e) {
                mostraMensagem('success', 'Tafera alterada com sucesso.');

                console.log(novoTarefaData);

                tarefaTitulo.html(novoTarefaTitulo);
                tarefaData.html(novoTarefaData);
                tarefaPrioridade.html(novoTarefaPrioridade);
                tarefaBotoes.html('<button class="btn" title="Editar" onclick="editarTarefa('   + tarefaCodigo + ')"><i class="fa fa-pencil"></i></button>' +
                                  '<button class="btn btn--danger" title="Remover" onclick="removerTarefa(' + tarefaCodigo + ')"><i class="fa fa-trash"></i></button>');
            }
        } else {
            var mensagem = 'Não foi possível alterar a tarefa.<br /><small>' + event.error.name + '</small>';
            mostraMensagem('danger', mensagem);
        }

        atualizarListaTarefas();
    }

    request.onerror = function(event) {
        var mensagem = 'Não foi possível alterar a tarefa.<br /><small>' + event.error.name + '</small>';
        mostraMensagem('danger', mensagem);
    }
}

function edicaoCancelar(tarefaCodigo) {
    var tarefaTitulo     = $('#tarefaTitulo_'     + tarefaCodigo);
    var tarefaData       = $('#tarefaData_'       + tarefaCodigo);
    var tarefaPrioridade = $('#tarefaPrioridade_' + tarefaCodigo);
    var tarefaBotoes     = $('#tarefaBotoes_'     + tarefaCodigo);

    var oldTarefaTitulo     = $('#editarTarefaTitulo_'     + tarefaCodigo).data('previousvalue');
    var oldTarefaData       = $('#editarTarefaData_'       + tarefaCodigo).data('previousvalue');
    var oldTarefaPrioridade = $('#editarTarefaPrioridade_' + tarefaCodigo).data('previousvalue');

    if( oldTarefaPrioridade == '3' ) {
        oldTarefaPrioridade = 'Alta';
    } else if( oldTarefaPrioridade == '2' ) {
        oldTarefaPrioridade = 'Média';
    } else {
        oldTarefaPrioridade = 'Baixa';
    }

    tarefaTitulo.html(oldTarefaTitulo);
    tarefaData.html(converteDataParaBrasil(oldTarefaData));
    tarefaPrioridade.html(oldTarefaPrioridade);
    tarefaBotoes.html('<button class="btn" title="Editar" onclick="editarTarefa(' + tarefaCodigo + ')"><i class="fa fa-pencil"></i></button>' +
                      '<button class="btn btn--danger" title="Remover" onclick="removerTarefa(' + tarefaCodigo + ')"><i class="fa fa-trash"></i></button>');
}

function removerTarefa(tarefaCodigo) {
    database.transaction(['tarefas'], 'readwrite').objectStore('tarefas').delete(tarefaCodigo).onsuccess = function(event) {
        mostraMensagem('success', 'Tarefa removida com sucesso.');

        atualizarListaTarefas();
    }
}


// busca
// =============================================================================
function buscarPorTitulo() {
    var tarefaBusca = $('#buscaTitulo').val();
    var ultimaBusca  = tarefaBusca.substr(0, tarefaBusca.length-1)+String.fromCharCode(tarefaBusca.charCodeAt(tarefaBusca.length-1)+1);
    var retornoBusca = IDBKeyRange.bound(tarefaBusca, ultimaBusca, false, true);

    if( tarefaBusca != '' && tarefaBusca.length > 2 ) {
        var request = database.transaction(['tarefas'], 'readonly').objectStore('tarefas').index('tarefa').openCursor(retornoBusca);

        $('#resultadoArea').removeClass('hide');
        $('#resultadoTitulo').html('Resultado da busca para "<strong>' + tarefaBusca + '</strong>"');
        $('#resultadoBusca').html('');

        request.onsuccess = listarResultado;
    } else {
        mostraMensagem('danger', 'Digite 3 ou mais caracteres.');
    }
}

function buscarPorData() {
    var dataBusca = $('#buscaDataLimite').val();
    var retornoBusca = IDBKeyRange.upperBound(dataBusca, false);

    if( dataBusca != '' || dataBusca.length > 9 ) {
        var request = database.transaction(['tarefas'], 'readonly').objectStore('tarefas').index('data').openCursor(retornoBusca);

        $('#resultadoArea').removeClass('hide');
        $('#resultadoTitulo').html('Resultado da busca até "<strong>' + converteDataParaBrasil(dataBusca) + '</strong>"');
        $('#resultadoBusca').html('');

        request.onsuccess = listarResultado;
    } else {
        mostraMensagem('danger', 'A data que você digitou é inválida.');
    }
}

function listarResultado(event) {
    var item = event.target.result;

    if(item) {
        var tituloCodigo     = item.primaryKey;
        var tituloTarefa     = item.value.tarefa;
        var tarefaData       = converteDataParaBrasil(item.value.data);
        var tarefaPrioridade = item.value.prioridade;

        if( tarefaPrioridade == '3' ) {
            tarefaPrioridade = 'Alta';
        } else if( tarefaPrioridade == '2' ) {
            tarefaPrioridade = 'Média';
        } else {
            tarefaPrioridade = 'Baixa';
        }

        $('#resultadoBusca')
            .append('<li class="lista-tarefas__item">' +
                '<strong><small>#' + tituloCodigo + '</small> ' +
                tituloTarefa + '</strong><br />' +
                tarefaData + ' - <i>(' + tarefaPrioridade + ')</i>' +
            '</li>');

       item.continue();
    }
}


// utils
// =============================================================================
function mostraMensagem(tipo, mensagem) {
    var alertArea = $('#alertarea');

    // escapar strings ¬¬
    alertArea.html(mensagem + '<button class="alert__close" onclick="fecharAlerta(\'' + alertArea.selector + '\', \'' + tipo + '\')">&times;</button>');
    alertArea.addClass('alert--' + tipo);

    window.setTimeout(function() { fecharAlerta(alertArea.selector, tipo) }, 10E3);

}

function fecharAlerta(elemento, tipoAlerta) {
    console.log(elemento + ' - ' + typeof elemento);
    console.log(tipoAlerta + ' - ' + typeof tipoAlerta);

    $(elemento).removeClass('alert--' + tipoAlerta).html('');
}

function converteDataParaBrasil(data) {
    var dataArray = data.split('-');

    return dataArray[2] + '/' + dataArray[1] + '/' + dataArray[0];
}

function converteDataParaPC(data) {
    var dataArray = data.split('/');

    return dataArray[2] + '-' + dataArray[1] + '-' + dataArray[0];
}

window.addEventListener('load', init(), false);